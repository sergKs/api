<?php

use yii\db\Migration;

/**
 * Handles the creation of table `books`.
 */
class m180525_185309_create_books_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('books', [
            'id' => $this->primaryKey(),
            'author_id' => $this->integer(),
            'title' => $this->string(),
            'year' => $this->integer(),
            'isbn' => $this->string(),
        ]);

        $this->createIndex(
            'idx-book-author_id',
            'books',
            'author_id'
        );

        $this->addForeignKey(
            'fk-book-author_id',
            'books',
            'author_id',
            'authors',
            'id',
            'RESTRICT',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex(
            'idx-book-author_id',
            'post'
        );

        $this->dropForeignKey(
            'fk-book-author_id',
            'post'
        );

        $this->dropTable('books');
    }
}
