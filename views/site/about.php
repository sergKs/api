<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'API help';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1> Методы API: </h1>

    <h3> API книг: </h3>

    <p>
        Получение списка книг: <code> метод: GET,  url: /books </code><br>
        Получение книги по идентификатору: <code> метод: GET,  url: /books/id </code><br>
        Обновление данных о книге: <code> метод: POST,  url: /books/id </code><br>
        Удаление книги: <code> метод: GET, url: /books/delete/id </code><br>
        Добавление новой книги: <code> метод: POST,  url: /books </code><br>
    </p>

    <h3> API авторов: </h3>

    <p>
        Получение списка авторов: <code> метод: GET,  url: /authors </code><br>
        Получение автора по идентификатору: <code> метод: GET,  url: /authors/id </code><br>
        Обновление данных об авторе: <code> метод: POST,  url: /authors/id </code><br>
        Удаление автора: <code> метод: GET,  url: /authors/delete/id </code><br>
        Добавление нового автора: <code> метод: POST,  url: /authors </code><br>
    </p>


</div>
