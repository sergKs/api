<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Authors;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BooksSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Books';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="books-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Book', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            [
                'attribute' => 'author_id',
                'label' => 'Автор',
                'content' => function($data){
                    return $data->getAuthorSename();
                },
                'filter' => Authors::getAuthorsList()
            ],
            'title',
            'year',
            'isbn',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
