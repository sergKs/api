<?php

namespace app\modules\api\controllers;

use yii\rest\Controller;
use app\models\Books;

/**
 * Book controller for the `api` module
 */
class BookController extends Controller
{
    // просмотр конкретного автора
    public function actionView($id)
    {
        return Books::findOne($id);
    }

    // просмотр всех авторов
    public function actionIndex()
    {
        return Books::find()->all();
    }

    // удаление автора
    public function actionDelete($id)
    {
        if (Books::findOne($id)->delete()) return ['book with id = '.$id.' deleted'];
        return ['error' => 'book with id = '.$id.' not deleted'];
    }

    // создание автора
    public function actionCreate()
    {
        $model = new Books();

        if ($model->load(\Yii::$app->getRequest()->getBodyParams(), '') && $model->save()) {
            return $model;
        }

        return ['error' => 'book not created'];
    }

    // обновление автора
    public function actionUpdate($id)
    {
        $model = Books::findOne($id);

        if ($model->load(\Yii::$app->getRequest()->getBodyParams(), '') && $model->save()) {
            return $model;
        }

        return ['error' => 'book with id = '.$id.' not updated'];
    }

}
