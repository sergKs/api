<?php

namespace app\modules\api\controllers;

use yii\rest\Controller;
use app\models\Authors;

/**
 * Author controller for the `api` module
 */
class AuthorController extends Controller
{
    // просмотр конкретного автора
    public function actionView($id)
    {
        return Authors::findOne($id);
    }

    // просмотр всех авторов
    public function actionIndex()
    {
        return Authors::find()->all();
    }

    // удаление автора
    public function actionDelete($id)
    {
        if (Authors::findOne($id)->delete()) return ['author with id = '.$id.' deleted'];
        return ['error' => 'author with id = '.$id.'  not deleted'];
    }

    // создание автора
    public function actionCreate()
    {
        $model = new Authors();

        if ($model->load(\Yii::$app->getRequest()->getBodyParams(), '') && $model->save()) {
            return $model;
        }

        return ['error' => 'author not created'];
    }

    // обновление автора
    public function actionUpdate($id)
    {
        $model = Authors::findOne($id);

        if ($model->load(\Yii::$app->getRequest()->getBodyParams(), '') && $model->save()) {
            return $model;
        }

        return ['error' => 'author with id = '.$id.' not updated'];
    }

}
